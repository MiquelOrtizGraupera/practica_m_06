package model;

import connexionsBD.ConexioEmpresa;

import java.sql.*;

public class Empleat {
    int emp_no;
    String cognom;
    String ofici;
    int cap;
    String data;
    int salari;
    int comisio;
    int dep;

    public Empleat(int emp_no, String cognom, String ofici, int cap, String data, int salari, int comisio, int dep) {
        this.emp_no = emp_no;
        this.cognom = cognom;
        this.ofici = ofici;
        this.cap = cap;
        this.data = data;
        this.salari = salari;
        this.comisio = comisio;
        this.dep = dep;
    }

    public static void InsertEmpleat(int emp_no, String cognom, String ofici, int cap, String data, int salari, Integer comisio, int dep){
        try{
            Connection conexio = ConexioEmpresa.ConectionEmpresa();

            PreparedStatement statement = conexio.prepareStatement("INSERT INTO emp VALUES(?,?,?,?,?,?,?,?)");
            statement.setInt(1,emp_no);
            statement.setString(2,cognom);
            statement.setString(3,ofici);
            statement.setInt(4,cap);
            statement.setDate(5, java.sql.Date.valueOf(data) );
            statement.setInt(6,salari);
            statement.setObject(7,comisio);
            statement.setInt(8,dep);

            statement.executeUpdate();

            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void ConsultarDadesEmpleat7788(){
        try{
            Connection conexio = ConexioEmpresa.ConectionEmpresa();
            Statement sentencia = conexio.createStatement();

            String query = "SELECT * FROM emp WHERE emp_no = 7788";
            boolean valor = sentencia.execute(query);

            if(valor){
                ResultSet resultat = sentencia.getResultSet();

                while(resultat.next()){
                    System.out.printf("EMPLEAT %n" +
                            "%d, %s, %s, %d, %s, %d, %d, %d %n",
                            resultat.getInt(1),resultat.getString(2),resultat.getString(3),
                            resultat.getInt(4),resultat.getString(5),resultat.getInt(6),
                            resultat.getInt(7),resultat.getInt(8));
                }
            }
            sentencia.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void EliminarEmpleat4885(){
        try{
            Connection conexio = ConexioEmpresa.ConectionEmpresa();
            Statement statement = conexio.createStatement();

            String query = "DELETE FROM emp WHERE emp_no = 4885";
            statement.execute(query);

        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
