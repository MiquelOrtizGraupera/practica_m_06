package model;

import connexionsBD.ConexioEmpresa;

import javax.xml.transform.Result;
import java.sql.*;

public class Client {
    int client_cod;
    String nom;
    String adres;
    String ciutat;
    String estat;
    int codi_postal;
    int area;
    String telefon;
    int representant_codi;
    double limit_credit;
    String description;

    public Client(int client_cod, String nom, String adres, String ciutat, String estat, int codi_postal, int area, String telefon, int representant_codi, double limit_credit, String description) {
        this.client_cod = client_cod;
        this.nom = nom;
        this.adres = adres;
        this.ciutat = ciutat;
        this.estat = estat;
        this.codi_postal = codi_postal;
        this.area = area;
        this.telefon = telefon;
        this.representant_codi = representant_codi;
        this.limit_credit = limit_credit;
        this.description = description;
    }

    public static void UpdateCreditClient(int client_cod, double limit_credit){
        try {
            Connection conexio = ConexioEmpresa.ConectionEmpresa();

            PreparedStatement statement = conexio.prepareStatement("UPDATE client SET limit_credit=? WHERE client_cod=?");
            statement.setDouble(1,limit_credit);
            statement.setInt(2,client_cod);

            statement.executeUpdate();
            statement.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void ConsultarDadesCLient106(){
        try {
            Connection conexio = ConexioEmpresa.ConectionEmpresa();
            Statement sentencia = conexio.createStatement();

            String query = "SELECT * FROM client WHERE client_cod = 106";
            boolean valor = sentencia.execute(query);

            if(valor){
                ResultSet resultat = sentencia.getResultSet();

                while (resultat.next()){
                    System.out.printf("CLIENT  %n" +
                            "%d, %s, %s, %s, %s, %d, %d, %s, %d, %2f, %s  %n",
                            resultat.getInt(1),resultat.getString(2),resultat.getString(3),resultat.getString(4),
                            resultat.getString(5),resultat.getInt(6),resultat.getInt(7),resultat.getString(8),
                            resultat.getInt(9),resultat.getDouble(10),resultat.getString(11));
                }
            }

            sentencia.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public static void EliminarClient(int client_cod){
        try{
            Connection conexio = ConexioEmpresa.ConectionEmpresa();
            PreparedStatement statement = conexio.prepareStatement("DELETE FROM client WHERE client_cod = ?");

            statement.setInt(1,client_cod);
            statement.executeUpdate();

            statement.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
