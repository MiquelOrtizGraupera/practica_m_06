package model;

import connexionsBD.ConexioEmpresa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Producte {
    int product_num;
    String descripcio;

    public Producte(int product_num, String descripcio) {
        this.product_num = product_num;
        this.descripcio = descripcio;
    }

    public static void InsertProductes(int product_num, String descripcio){
        try{
           Connection conexio = ConexioEmpresa.ConectionEmpresa();

            PreparedStatement statement = conexio.prepareStatement("INSERT INTO producte VALUES(?,?)");
            statement.setInt(1, product_num);
            statement.setString(2,descripcio);
            statement.executeUpdate();

            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void ConsultarProductes(int cod_prod){
        try {
            Connection conexio = ConexioEmpresa.ConectionEmpresa();

            PreparedStatement statement = conexio.prepareStatement("SELECT * FROM producte WHERE prod_num = ?");
            statement.setInt(1,cod_prod);

            boolean valor = statement.execute();

            if(valor){
                ResultSet resultat = statement.getResultSet();

                while(resultat.next()){
                    System.out.printf("PRODUCTE %n" +
                            "%d, %s",resultat.getInt(1),resultat.getString(2));
                }
            }
            statement.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static void EliminarProductePreparedStatement(int product_num){
        try{
            Connection conexio = ConexioEmpresa.ConectionEmpresa();
            PreparedStatement statement = conexio.prepareStatement("DELETE FROM producte WHERE prod_num = ?");
            statement.setInt(1,product_num);
            statement.execute();

            statement.close();
            conexio.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
