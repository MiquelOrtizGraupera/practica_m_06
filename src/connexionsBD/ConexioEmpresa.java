package connexionsBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexioEmpresa {

    public static Connection ConectionEmpresa() throws SQLException {
        Connection conexio = DriverManager.getConnection("jdbc:postgresql://192.168.56.102:5432/empresa", "empresa", "empresa");
        return conexio;
    }
}
