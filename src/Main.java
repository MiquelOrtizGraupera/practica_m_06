import model.Client;
import model.Empleat;
import model.Producte;


public class Main {
    public static void main(String[] args) {
        /*
          EXERCICI 1 --> SCRIPT CARREGAT AMB ÉXIT
          */

        /*
          EXERCICI 2 --> INSERT 3 PRODUCTES
         */

        Producte.InsertProductes(300388,"RH GUIDE TO PADDLE");
        Producte.InsertProductes(400552,"RH GUIDE TO BOX");
        Producte.InsertProductes(400333,"ACE TENNIS BALLS-10 PACK");

        /*
          EXERCICI 3 -->  INSERT 3 EMPLEATS
         */

        Empleat.InsertEmpleat(4885,"BORREL","EMPLEAT",7902,"1981-12-25",104000,null,30);
        Empleat.InsertEmpleat(8772,"PUIG","VENEDOR",7698,"1990-01-23",108000,null,30);
        Empleat.InsertEmpleat(9945,"FERRER","ANALISTA",7698,"1988-05-17",169000,39000,20);
        /*
           EXERCICI 4 --> UPDATE LIMIT CREDIT
         */

        Client.UpdateCreditClient(104,20000);
        Client.UpdateCreditClient(106,12000);
        Client.UpdateCreditClient(107,20000);

        /*
          EXERCICI 5 --> CONSULTA LES DADES CLIENT 106
         */
        Client.ConsultarDadesCLient106();

        /*
         EXERCICI 6 --> CONSULTAR LES DADES EMPLEAT 7788
         */
        Empleat.ConsultarDadesEmpleat7788();

        /*
          EXERCICI 7 --> CONSULTAR DADES PRODUCTE 101860 AMB PREPARED STATEMENT
         */
        Producte.ConsultarProductes(101860);

        /*
          EXERCICI 8 --> Eliminar el client amb codi 109 utilitzant el PreparedStatement
         */

        Client.EliminarClient(109);

        /*
          EXERCICI 9 --> Eliminar l’empleat amb codi 4885.
         */

        Empleat.EliminarEmpleat4885();

        /*
          EXERCICI 10 --> Eliminar el producte 400552 utilitzant el PreparedStatement
         */
        Producte.EliminarProductePreparedStatement(400552);

    }
}
